<?php

class Sagra_Piatto_Stato_Tipo implements Sagra_Tipo_Interfaccia
{
    const DISATTIVATO = 0;
    const A_MENU = 1;
    
    public static function ottScelte()
    {
        return [
            self::DISATTIVATO       => 'Disattivato',
            self::A_MENU            => 'A menù',
        ];
    }
}

