<?php
class Piatto{
	// VAR	
		private $connessione;
		private $id;
		private $descrizione;
		private $prezzo;
		private $quantita;
		private $tipologia;
		
	// METODI	
		 function __construct($d , $p , $t, $c ){
			
			$this->connessione = $c;
			$this->descrizione = $d;
			$this->tipologia = $t;
			$this->prezzo = $p;
		}
		
		// INIZIO GET
			public function getConnessione(){
				
				return $this->connessione;
			}
			
			public function getId(){
				
				return $this->id;
			}
			
			public function getDescrizione(){
				
				return $this->descrizione;
			}
			
			public function getPrezzo(){
				
				return $this->prezzo;
			}
			
			public function getQuantita(){
				
				return $this->quantita;
			}
			
			public function getTipologia(){
				
				return $this->tipologia;
			}
		// FINE GET
		
		// INIZIO SET	
			public function setConnessione($valore){
				
				$this->connessione = $valore; 
			}
			
			public function setId($valore){
				
				$this->id = $valore; 
			}
			
			public function setDescrizione($valore){
				
				$this->descrizione = $valore; 
			}
			
			public function setPrezzo($valore){
				
				$this->prezzo = $valore; 
			}
			
			public function setQuantita($valore){
				
				$this->quantita = $valore; 
			}
			
			public function setTipologia($valore){
				
				$this->tipologia = $valore; 
			}
		// FINE SET	
		
		// Salva nel db il piatto e ritorna l'ID
			public function setPiattoDb(){
				
				$c = $this->getConnessione();
				
				$insert = "
							INSERT INTO 
							
								piatti(descrizione, prezzo, id_tipologia) 
							
							VALUES ('".$this->getDescrizione()."',".$this->getPrezzo().",".$this->getTipologia().")
						  ";
						  
				if(!mysqli_query($db, $insert, $c)){
					
					echo "Errore nell'inserimento";
				}			
				
				$i = mysqli_insert_id($c);
				
				return $i;
			} 
		
		// Recupera i dati del piatto dato l'ID
		public function getPiattoDb($i){
			
			$c = $this->getConnessione();
			
			$select = "
					  	SELECT 
								*
						FROM
								piatti
						WHERE
								id_piatto = $i
					  ";
				  
			$result = mysqli_query($db, $select,$c);
			
			while($row = mysqli_fetch_array($result)){
				
				$this->setDescrizione($row['descrizione']);
				$this->setPrezzo($row['prezzo']);
				$this->setTipologia($row['id_tipologia']);
				
			}
						  
		}
		
		// Calcola prezzo (P x Q)
		public function calcolaPrezzo(){
			 
			return ($this->getPrezzo())*($this->getQuantita());
		}
		
		
		// imposta piatto come contatore
		public function setContatore($i, $g, $s){
			
			$c = $this->getConnessione();
			
			$update = "UPDATE piatti SET contatore=1,giacenza= $g, salto=$s WHERE id_piatto=$i";
			
			if(!mysqli_query($db, $update, $c)) echo "Errore nell'inserimento";
			
		}
}


class Ordine{
	// VAR	
		//array di piatti
		// ID
		// totale
		// totale food
		// totale no food
		// nominativo
		
	// METODI	
		// TO DO : stampa ordine
		// TO DO : calcola tot
		// TO DO : calcola tot food
		// TO DO : calcola tto no food	
}

class Sagra{
	// VAR	
		private $connessione;
		private $id;
		private $descrizione;
		private $data_inizio;
		private $data_fine;
		private $luogo;
		private $contatori[];
		// array di piatti per contatori(?)
		
		// INIZIO GET
			public function getConnessione(){
				
				return $this->connessione;
			}
			
			public function getId(){
				
				return $this->id;
			}
			
			public function getDescrizione(){
				
				return $this->descrizione;
			}
		
		
		// INIZIO SET	
			public function setConnessione($valore){
				
				$this->connessione = $valore; 
			}
			
			public function setId($valore){
				
				$this->id = $valore; 
			}
			
			public function setDescrizione($valore){
				
				$this->descrizione = $valore; 
			}
			
		
	// METODI	
		// TO DO : stampa listino
		// TO DO : statistiche(coperti,contatori,ecc)
		// TO DO : personalizza dati sagra
		// TO DO : imposta contatori
		// TO DO : stampa contatori
		
}

?>
