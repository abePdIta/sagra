<?php
	$db = null; include_once("../include/connessione.php");   /* @var $db mysqli */
	
	$id_sagra = $_GET['id'];
	
	$id_serata = $_GET['id_serata'];
	
	$select = "SELECT * FROM piatti";
	
	
	$result = mysqli_query($db, $select);
	
	$select_html = "<select name=\"id_piatto\" ><option value=\"0\">-</option>
";
	
	while($row = mysqli_fetch_array($result)){
		
		$select_html.="

              		<option value=\"".$row['id_piatto']."\">".$row['descrizione']."</option>";
                   
               
				
	}
	
	$select_html.=" </select>";
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link rel=stylesheet href="../css/style.css" type="text/css">
    </head>
    
    <body>
    	
		<?php include_once("../include/testata.php"); ?>
        
        <?php include_once("../include/menu.php"); ?>
        
        <div class="titolo_configurazione">Nuovo contatore</div> 
        
        <div id="corpo_configurazione">
          
          <form action="insert_contatore.php" method="post">
              
              <input type="hidden" name="id_serata" value="<?php echo $id_serata;?>" />
              
              <div class="riga_configurazione">
                
                <div class="label_configurazione">Piatto</div>
                
                <?php echo $select_html; ?>
              
              </div>
              
              
               <div class="riga_configurazione">
                
                <div class="label_configurazione">Magazzino</div>
                
                <input type="text" name="magazzino" class="input_configurazione" value="0" />
              
              </div>
              
               
              
              <div class="riga_configurazione">
              
                <button id="button_configurazione" type="submit">Salva</button>
              
              </div>
              
              </form>
        </div>
        
    </body>
</html>