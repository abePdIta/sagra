<?php

/**
 * 
 * @author abe
 * @property-read int	$totalePorzioni
 */
class Sagra_Preparazione_Piatti {
	public $nome;
	public $porzioniInCoda;
	public $porzioniServite;
	
	/**
	 * 
	 * @param string $nome
	 * @param int $porzioniInCoda
	 * @param int $porzioniServite
	 */
	public function __construct($nome, $porzioniInCoda, $porzioniServite) {
		$this->nome = $nome;
		$this->porzioniInCoda = $porzioniInCoda;
		$this->porzioniServite = $porzioniServite;
	}
	
	public function __get($nome) {
		switch ($nome) {
			case 'totalePorzioni':
				return $this->porzioniInCoda + $this->porzioniServite;
				
			default:
				throw new Exception(sprintf('Si sta cercando di accedere ad una proprietà (%s) che non esiste per la classe %s.', $nome, __CLASS__));
		}
	}
}