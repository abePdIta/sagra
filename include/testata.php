<?php
	include_once("connessione.php");
	
	$select = "SELECT 
					DATE_FORMAT(data_inizio, '%d/%m/%Y') as data_inizio,
					DATE_FORMAT(data_fine, '%d/%m/%Y') as data_fine,
					descrizione,
					luogo,
					id_sagra
					
			   FROM sagre WHERE id_stato = 1";
	
	$result = mysqli_query($db, $select);
	
	while($row = mysqli_fetch_array($result)){
		$d_i = $row['data_inizio'];
		$d_f = $row['data_fine'];
		
		
		
		
		$descrizione = $row['descrizione'];
		$luogo = $row['luogo'];
		$id_sagra = $row['id_sagra'];
	}
?>
<div id="testata">
	<div id="intestazione_descrizione">
		<?php echo $descrizione; ?>
    </div>
	
	<div id="intestazione_luogo">
		<?php echo $luogo;?>
    </div>
    
    <div id="intestazione_date">
		Dal <?php echo $d_i;?>  Al <?php echo $d_f;?> 
    </div>
</div>
