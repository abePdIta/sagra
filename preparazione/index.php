<?php

require_once '../classi/Sagra_Principale.php';

$pagina = new Sagra_Preparazione_IndexPagina();



$tipologie = $pagina->ottParametro('tipologie');

$pagina->impTitolo('Postazione '. ($tipologie == 1? 'bevande': 'cucina'));

$bd = Sagra_Principale::ottCollegamentoBasedati();

$query = <<<SQL
SELECT DISTINCT id_ordine, totale, evaso, asporto
	FROM ordini 
	LEFT JOIN `piatti_ordini` USING (`id_ordine`) 
	LEFT JOIN `piatti` USING (id_piatto) 
	WHERE id_serata = ? AND id_tipologia IN (?)
	ORDER BY totale DESC
SQL;
$istruzione = $bd->preparaEInserisciParametri($query, 'iI', [Sagra_Principale::ottIdSerataCorrente(), explode(',', $tipologie)]);

$istruzione->execute();
$risultato = $istruzione->get_result();	/* @var $risultato mysqli_result */
$istruzione->close();

$ordiniRimanenti = [];
$ordiniAsportoRimanenti = [];
$entropia = 0;
while ($ordine = $risultato->fetch_object(Sagra_Preparazione_OrdineInCoda::class)) {
	/* @var $ordine Sagra_Preparazione_OrdineInCoda */
	if ($ordine->evaso) {
		$entropia++;
	}
	else {
		$ordine->entropia = $entropia;
		if ($ordine->asporto) {
		    $ordiniAsportoRimanenti[$ordine->id_ordine] = $ordine;
		}
		else {
		    $ordiniRimanenti[$ordine->id_ordine] = $ordine;
		}
	}
}

$pagina->totaleOrdiniRimanenti = count($ordiniRimanenti) + count($ordiniAsportoRimanenti);
$pagina->arrayOrdiniInCoda = $ordiniRimanenti;
$pagina->arrayOrdiniAsportoInCoda = $ordiniAsportoRimanenti;



$query = <<<SQL
SELECT SUM(quantita) AS qta, evaso, descrizione, id_piatto 
	FROM `piatti` 
	LEFT JOIN `piatti_ordini` USING (`id_piatto`) 
	LEFT JOIN  `ordini` USING (id_ordine) 
	WHERE id_serata = ? AND id_tipologia IN (?)
	GROUP BY `id_piatto`, `evaso`
    ORDER BY id_tipologia, descrizione
SQL;
$istruzione = $bd->preparaEInserisciParametri($query, 'iI', [Sagra_Principale::ottIdSerataCorrente(), explode(',', $tipologie)]);

$istruzione->execute();
$descrizione = null; $quantita = null; $evaso = null; $id = null;
$istruzione->bind_result($quantita, $evaso, $descrizione, $id);

/* @var $arrayPiatti Sagra_Preparazione_Piatti[] */
$arrayPiatti = [];
while ($istruzione->fetch()) {
	if (isset($arrayPiatti[$id])) {
		if ($evaso) {
			$arrayPiatti[$id]->porzioniServite += $quantita;
		}
		else {
			$arrayPiatti[$id]->porzioniInCoda += $quantita;
		}
	}
	else {
		if ($evaso) {
			$arrayPiatti[$id] = new Sagra_Preparazione_Piatti($descrizione, 0, $quantita);
		}
		else  {
			$arrayPiatti[$id] = new Sagra_Preparazione_Piatti($descrizione, $quantita, 0);
		}
	}
}
$istruzione->close();

$pagina->arrayPiatti = $arrayPiatti;

if ($pagina->ottParametro('ajax')) {
    $pagina->impScriptVista('ordiniEPiattiInCoda.phtml');
    
    $pagina->inserisciContenutoPrincipale();
}
else {
    $pagina->azioneStampa = 'stampa.php';
    
    $pagina->tipologie = $tipologie;
    
    $pagina->rendi();
}