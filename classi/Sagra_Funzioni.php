<?php

class Sagra_Funzioni {
	public static function strEnnesimaPos($pagliaio, $ago, $n, $ignoraMaiuscole = false)
	{
		if ($ignoraMaiuscole) {
			$pagliaio = strtolower($pagliaio);
			$ago = strtolower($ago);
		}
		$ct = 0;
		$pos = 0;
		while (($pos = strpos($pagliaio, $ago, $pos)) !== false) {
			if (++$ct == $n) {
				return $pos;
			}
			$pos++;
		}
		return false;
	}  
}