<?php

class Sagra_Preparazione_OrdineInCoda extends Sagra_Ordine{
	const 	ENTROPIA_GIALLO = 1,
			ENTROPIA_ROSSO = 3;
	/**
	 * L'entropia cresce a seconda del numero di ordini più recenti che vengono evasi prima
	 * Si può tradurre anche con "frustrazione" ;)
	 * @var int
	 */
	public $entropia;
}