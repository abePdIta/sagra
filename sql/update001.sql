-- aggiunto campo evaso a tabella ordini
ALTER TABLE `ordini` ADD `evaso` BOOLEAN NOT NULL DEFAULT FALSE AFTER `id_serata`;

-- aggiunta tabella variabili
CREATE TABLE `sagre`.`variabili` ( `nome` TEXT NOT NULL , `valore` TEXT NULL DEFAULT NULL , PRIMARY KEY (`nome`(8))) ENGINE = InnoDB;

INSERT INTO `variabili` (`nome`, `valore`) VALUES ('versione-database', '000');

-- tabella ordini, campi totale
ALTER TABLE `ordini` CHANGE `tot_bevande` `tot_bevande` DECIMAL(5,2) NULL DEFAULT NULL;
ALTER TABLE `ordini` CHANGE `tot_food` `tot_food` DECIMAL(5,2) NULL DEFAULT NULL;

-- aggiornamento numero versione
UPDATE `variabili` SET `valore` = '001' WHERE `variabili`.`nome` = 'versione-database';



-- [commento] triviale, ma utile
ALTER TABLE `piatti` CHANGE `id_tipologia` `id_tipologia` INT(10) NOT NULL COMMENT '1: bevande, 2: primi, 3: secondi, 4: contorni, 5: menu';
