-- Anggiungere tempi tabella ordini

ALTER TABLE `ordini`  ADD `time_inserimento` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	ADD `time_stampa_bevande` DATETIME NULL DEFAULT NULL,
	ADD `time_stampa_cibo` DATETIME NULL DEFAULT NULL;
	
	

-- aggiornamento numero versione
UPDATE `variabili` SET `valore` = '004' WHERE `variabili`.`nome` = 'versione-database';
