<?php

require_once '../classi/Sagra_Principale.php';

$pagina = new Sagra_Preparazione_StampaPagina();



$tipologie = $pagina->ottParametro('tipologie');

$pagina->impTitolo('Stampa '. ($tipologie == 1? 'bevande': 'cucina'));

$bd = Sagra_Principale::ottCollegamentoBasedati();

$numeroOrdine = $pagina->ottParametro('numero-ordine');

if ($numeroOrdine) {
    $query = <<<SQL
    SELECT *
    	FROM ordini
    	LEFT JOIN `piatti_ordini` USING (`id_ordine`)
    	LEFT JOIN `piatti` USING (id_piatto)
    	WHERE totale = ? AND id_serata = ? AND id_tipologia IN (?)
    	ORDER BY id_ordine LIMIT 1
SQL;
    
    $istruzione = $bd->preparaEInserisciParametri($query, 'iiI', [$numeroOrdine, Sagra_Principale::ottIdSerataCorrente(), explode(',', $tipologie)]);
}
else {
    $query = <<<SQL
    SELECT *
    	FROM ordini
    	LEFT JOIN `piatti_ordini` USING (`id_ordine`)
    	LEFT JOIN `piatti` USING (id_piatto)
    	WHERE evaso = FALSE AND id_serata = ? AND id_tipologia IN (?)
    	ORDER BY id_ordine LIMIT 1
SQL;
    
    $istruzione = $bd->preparaEInserisciParametri($query, 'iI', [Sagra_Principale::ottIdSerataCorrente(), explode(',', $tipologie)]);
}

$istruzione->execute();

$risultato = $istruzione->get_result();	/* @var $risultato mysqli_result */
$istruzione->close();

if ($ordine = $risultato->fetch_object(Sagra_Ordine::class)) {
	/* @var $ordine Sagra_Ordine */
	$piatti = $ordine->piatti;
	$idPiatti = [];
	foreach ($piatti as $chiave => $piatto) {
		if (in_array($piatto->id_tipologia, explode(',', $tipologie))) {
		    $idPiatti[] = $piatto->id_piatto;
		}
		else {
			unset($piatti[$chiave]);
		}
	}
	$pagina->piatti = $piatti;
	
	$query = 'UPDATE piatti_ordini SET evaso = TRUE WHERE id_ordine = ? AND id_piatto IN (?)';
	$istruzione = $bd->preparaEInserisciParametri($query, 'iI', [$ordine->id_ordine, $idPiatti]);
	
	$istruzione->execute();
	$istruzione->close();
	
	$stampa = ($tipologie == 1? 'bevande': 'cibo');
	$query = "UPDATE ordini SET time_stampa_$stampa = CURRENT_TIMESTAMP WHERE id_ordine = ? AND time_stampa_$stampa IS NULL";
	$istruzione = $bd->preparaEInserisciParametri($query, 'i', [$ordine->id_ordine]);
	
	$istruzione->execute();
	$istruzione->close();
}


$pagina->ordine = $ordine;

$pagina->azioneRiepilogo = 'index.php';

$pagina->tipologie = $tipologie;

$pagina->rendi();