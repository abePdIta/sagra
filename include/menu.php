<script type="text/javascript" src="/sagre/jquery/jquery.js"></script>  
<!-- script type="text/javascript" src="/sagre/jquery/jquery.onfocus.js"></script--> 

<script src="/sagre/jquery/jquery.easing.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		//$('#menu').focus();
		 $('#menu ul').removeClass().addClass("subMenu-off");

        $('#menu li a').on('click mouseover', function() {
            $(this).next("ul").removeClass().addClass("subMenu-on");
        });
        $('#menu li').mouseleave(function() {
            $(this).children("ul").removeClass().addClass("subMenu-off");
            //console.log('mouseout')
        });
	});
</script>



<div id="sfondo_menu">
    <ul id="menu">
        
        <li >
        	<a href="#" id="prima">Configurazione</a>
            <ul class="voce_menu">
                <li>
                	<a href="/sagre/configurazione/visualizza_evento.php" >
                    	<span class="voce">Evento</span>
                    </a>    
                </li>
                
                <li>
                	<a href="/sagre/configurazione/inserimento_cassieri.php" >
                    	<span class="voce">Inserimento cassieri</span>
                    </a>    
                </li>
                
                <li>
                	<a href="/sagre/configurazione/visualizza_serate.php?id=<?php echo $id_sagra; ?>" >
                    	<span class="voce">Serate</span>
                    </a>    
                </li>
                
                <li>
                    <a href="/sagre/configurazione/visualizza_piatti.php?id=<?php echo $id_sagra; ?>">
                   		<span class="voce">Piatti</span>
                    </a>
                </li>
                
                
                <li>
                    <a href="/sagre/configurazione/visualizza_serate_contatori.php?id=<?php echo $id_sagra; ?>" >
                    	<span class="voce">Contatori</span>
                    </a>
                </li>
                
            </ul>
        </li>
        
        
        <li>
        	<a href="#" id="prima">Gestione</a>
            <ul  class="voce_menu">
                <li>
                    <a href="/sagre/ordini/ordini.php" >
                    <span class="voce">Ordini</span>
                    </a>
                </li>
                
                <li>
                    <a href="#" >
                    <span class="voce">Elimina Ordini</span>
                    </a>
                </li>
                
            </ul>
    	</li>
        
        
        <li >
        	<a href="#" id="prima">Statistiche</a>
            <ul class="voce_menu">
                <li>
                    <a href="/sagre/ordini/riepilogo_serate.php?id=<?php echo $id_sagra; ?>" >
                       <span class="voce">Riepilogo serata</span>
                    </a>
                </li>
                
                <li>
                    <a href="/sagre/ordini/riepilogo_contatori.php?id=<?php echo $id_sagra; ?>" >
                    	<span class="voce">Contatori</span>
                    </a>
                </li>
                
            </ul>
        </li>
        
    </ul>
</div>