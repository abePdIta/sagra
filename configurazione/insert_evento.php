<?php

    require_once '../classi/Sagra_Principale.php';
    
    
	
	$db = Sagra_Principale::ottCollegamentoBasedati();
	//include_once("../classi/sagra.php"); // mai usata 
	
	$descrizione = htmlentities($_POST['descrizione'], ENT_QUOTES,'UTF-8');
        $descrizione = addslashes($descrizione);
	$luogo = htmlentities($_POST['luogo'],ENT_QUOTES,'UTF-8');
        $luogo = addslashes($luogo);
	$data_inizio = $_POST['data_inizio'];
	$data_fine = $_POST['data_fine'];
	
	$update = "UPDATE sagre SET id_stato = 0"; // id_stato a zero dappertutto
	
	mysqli_query($db, $update);
	
	$a = explode("/",$data_inizio);
	
	$b = explode("/",$data_fine);
	
	$stamp_data_inizio = date("Y-m-d",mktime(0,0,0,intval($a[1]),intval($a[0]),intval($a[2]))); 
	$stamp_data_fine =  date("Y-m-d",mktime(0,0,0,intval($b[1]),intval($b[0]),intval($b[2]))); 
	
	
	$insert = "INSERT INTO sagre(descrizione,luogo,data_inizio,data_fine,id_stato) VALUES ('$descrizione','$luogo','$stamp_data_inizio','$stamp_data_fine',1)";
	
	//echo $insert;
	mysqli_query($db, $insert);
	
	$last_id = mysqli_insert_id($db);
	
	
	
	//calcolo serate
	$data1 = mktime(0,0,0,$b[1],$b[0],$b[2]);
	
	$data2 = mktime(0,0,0,$a[1],$a[0],$a[2]);
	
	$data = $data1 - $data2;
	
	$n_serate = (int)abs($data/86400) + 1; 
	
	
	
	$data = $stamp_data_inizio;
	
	for($i = 1; $i <= $n_serate; $i++){
		
		
		
		$ins = "INSERT INTO serate(numero,data,id_sagra) VALUES ($i,'$data',$last_id)";
		//echo $ins;
		$successo = mysqli_query($db, $ins);
		
		if (!$successo) {
		    $errore = mysqli_error($db);
		    $numero = mysqli_errno($db);
		    Sagra_Log::debug('Errore MySQL', ['errore-num' => $numero, 'messaggio' => $errore]);
		}
		
		$data = date("Y-m-d", mktime(0,0,0,$a[1],$a[0] +$i,$a[2]));
	}
	
	
	
	
	header("Location: http://".$_SERVER['SERVER_NAME']."/sagre/index.php");
?>
