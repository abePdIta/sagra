-- Anggiungere asporto tabella ordini

ALTER TABLE `ordini` ADD `asporto` BOOLEAN NOT NULL DEFAULT FALSE AFTER `coperti`, ADD INDEX `asporto` (`asporto`);
	
	

-- aggiornamento numero versione
UPDATE `variabili` SET `valore` = '005' WHERE `variabili`.`nome` = 'versione-database';
