<?php
	
	$db = null; include_once("../include/connessione.php");   /* @var $db mysqli */
	
	$id_sagra = $_GET['id_sagra'];
	
	$id_serata = $_GET['id_serata'];
	
	
	$select_s = "SELECT * FROM serate WHERE id_serata=$id_serata";
	
	$result_s = mysqli_query($db, $select_s);
	
	while($row_s = mysqli_fetch_array($result_s)){
		$fondo = $row_s['fondo_cassa'];
	}	
	// TO DO se la serata è dopo la prima il fondo cassa è il fondo cassa piu il guadagno
	
	
	$select = "SELECT * FROM cassieri";
	
	$result = mysqli_query($db, $select);
	
	$box = "";
	
	$i = 0;
	
	while($row = mysqli_fetch_array($result)){
		
		$box .= "<input type=\"checkbox\" id=\"cassieri\" name=\"cassieri[".$i."]\" value=\"".$row['id_cassiere']."\"   /> ".$row['cognome']." ".$row['nome']."<br />";
	
		$i++;
	}
	

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link rel=stylesheet href="../css/style.css" type="text/css">
    </head>
    
    <body>
    	
		<?php include_once("../include/testata.php"); ?>
        
        <?php include_once("../include/menu.php"); ?>
        
        <div class="titolo_configurazione">Configurazione serata</div> 
        
        <div id="corpo_configurazione">
          
          <form action="update_serata.php" method="post">
              
              <input type="hidden" name="id_serata" value="<?php echo $id_serata; ?>"  />
              
              <input type="hidden" name="id_sagra" value="<?php echo $id_sagra; ?>"  />
              	
              <div class="riga_configurazione">
                
                <div class="label_configurazione">Fondo cassa</div>
                
                <input type="text" name="fondo_cassa" class="input_configurazione" value="<?php echo $fondo; ?>" />
              
              </div>
              
              
               <div class="riga_configurazione">
                
                <div class="label_configurazione">Cassieri</div>
                
                <?php echo $box; ?>
              
              </div>
              
              
              <div class="riga_configurazione">
              
                <button id="button_configurazione" type="submit">Salva</button>
              
              </div>
              
              </form>
        </div>
        
    </body>
</html>
