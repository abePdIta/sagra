<?php
	
	$db = null; include_once("../include/connessione.php");   /* @var $db mysqli */
	
				include_once '../include/funzioni.php';
	
	// insert ordine
	
	$nominativo = Funzioni::getParameter('nominativo_hidden');
	
	$coperti = Funzioni::getParameter('coperti_hidden');
	
	$asporto = Funzioni::getParameter('asporto_hidden');
	
	$sconto = Funzioni::getParameter('sconto_hidden');
	
	$anteprima = Funzioni::getParameter('anteprima_hidden');
	//echo "--------- ". $sconto;
	
	
	$tot_scontato = Funzioni::getParameter('tot_sconto_hidden');
	
	// Percentuale non è mai usata
	//$percentuale = Funzioni::getParameter('percentuale');
	//echo "--------- ".$percentuale;

	
	$id_serata = Funzioni::getParameter('id_serata');
	
	
	//$id = Funzioni::getParameter('id_hidden'); // questo è da considerarsi obsoleto
	
	if ($id_serata) {
	
		$data_serata = "";
		
		$select_ser = "SELECT * FROM serate WHERE id_serata=$id_serata";
		
		$result_ser = mysqli_query($db, $select_ser);
			
		while($row_ser = mysqli_fetch_array($result_ser)){
			
			$data_serata = $row_ser['data'];
		}
	
		$dd = explode("-",$data_serata);
	} 
	else {
		$dd = ['', '', ''];
	}
	
	//selezione dati evento
	$query = "SELECT id_sagra as m FROM sagre WHERE id_stato = 1";
        $result = mysqli_query($db, $query);

	while($row = mysqli_fetch_array($result)){

		$id_s = $row['m'];
	}

	$select_evento = "SELECT * FROM sagre where id_sagra=$id_s";
	
	$result_evento = mysqli_query($db, $select_evento);
		
	$row_evento = mysqli_fetch_row($result_evento);
	
	$evento = $row_evento[1];
	
	$luogo = $row_evento[2];
	
	$d_i = explode("-",$row_evento[3]);

	$inizio = $d_i[2]."-".$d_i[1]."-".$d_i[0];
	
	$d_f = explode("-", $row_evento[4]);

	$fine = $d_f[2]."-".$d_f[1]."-".$d_f[0];
	
	
	
	$stampa_bevande = "";
	
	$tot_bevande = 0;
	$tot_primi= 0;
	$tot_secondi= 0;
	$tot_contorni= 0;
	$tot_menu= 0;
	
	$a = explode(";",$_POST['st']);
	
	$nominativo = mysqli_escape_string($db, $nominativo);
	$coperti = mysqli_escape_string($db, $coperti);
	$id_serata = mysqli_escape_string($db, $id_serata);
	$asporto = mysqli_escape_string($db, $asporto);
	
	// insert ordine
	if (! $anteprima) {
    	//echo "M ".$id_serata;
    	$insert_o = "INSERT INTO ordini (nominativo,coperti,id_serata,asporto,totale) VALUES ('$nominativo',$coperti,$id_serata,$asporto,(SELECT * FROM (SELECT IF(MAX(totale) IS NULL, 1, MAX(totale) + 1) FROM ordini WHERE id_serata = $id_serata) AS temp))";
    	
        mysqli_query($db, $insert_o);
	
	    $id_ordine_inserito = mysqli_insert_id($db);
	}
	
	// fine insert ordine
	
	// per ogni coppia (id,quantita)
	
	//bevande
	
	for($j=0;$j<sizeof($a)-1;$j++){
		
		$b = explode(",",$a[$j]);
		
		$id_piatto = $b[0];
		
		$quantita_piatto = $b[1];
		//echo "---".$quantita_piatto;
		
		// insert in piatti ordine
		if (! $anteprima) {
		    $insert_p = "INSERT INTO piatti_ordini(id_piatto,id_ordine,quantita) VALUES ($id_piatto,$id_ordine_inserito,$quantita_piatto)";
    		
    		mysqli_query($db, $insert_p);
		}
		
		
		$select_bevande = "SELECT * FROM piatti WHERE id_piatto=$id_piatto AND id_tipologia=1";
		
		
		$result_bevande = mysqli_query($db, $select_bevande);
		
		while($row_bevande = mysqli_fetch_array($result_bevande)){
			
			$prezzo_bevande = $row_bevande['prezzo'];
			
			$prezzo_tot_bevande = $prezzo_bevande*$quantita_piatto;
			$stampa_bevande .= "<div id=\"riga_stampa\">";
			$stampa_bevande .= "
                <div id=\"q\" class=\"fl\">
                     $quantita_piatto
                </div>
                
                <div id=\"piatto\" class=\"fl\">".$row_bevande['descrizione']."
                	
                </div>
                
                <div id=\"prezzo\" class=\"fl\">
                € ".$prezzo_tot_bevande."  
                </div>"
                ;
				$stampa_bevande .= "</div>";
				
			$tot_bevande = $tot_bevande + ($prezzo_tot_bevande);
		}
	
		if (! $anteprima) {
    		// contatori: decremento
    		$select_c = "SELECT * FROM contatori_serate WHERE id_piatto=$id_piatto AND id_serata=$id_serata AND id_stato=1";
    		//echo $select_c;
    		
    		$result_c = mysqli_query($db, $select_c);
    		
    		$num_righe = mysqli_num_rows($result_c);
    		
    		$magazzino = 0;
    		
    		if($num_righe>0){
    		
    			while($row_c=mysqli_fetch_array($result_c)){
    			
    				$magazzino = $row_c['magazzino'];
    				$ordinato = $row_c['ordinato'];
    				
    			}
    			$magazzino = $magazzino - $quantita_piatto;
    			$ordinato = $ordinato + $quantita_piatto;
    			
    			$up = "UPDATE contatori_serate SET magazzino= $magazzino,ordinato=$ordinato WHERE id_piatto=$id_piatto AND id_serata=$id_serata AND id_stato=1";
    			
    			mysqli_query($db, $up);
    		}
		}
	}
	
	//$tot_bevande;
	
	// fine bevande
	
	//primi
	
	$stampa_primi = "";
	
	for($j=0;$j<sizeof($a)-1;$j++){
		
		$b = explode(",",$a[$j]);
		
		$id_piatto = $b[0];
		
		$quantita_piatto = $b[1];
	
		
		
		$select_primi = "SELECT * FROM piatti WHERE id_piatto=$id_piatto AND id_tipologia=2";
		
		
		$result_primi = mysqli_query($db, $select_primi);
		
		
		while($row_primi = mysqli_fetch_array($result_primi)){
			$prezzo_primi = $row_primi['prezzo'];	
			$stampa_primi .= "<div id=\"riga_stampa\">";
			$stampa_primi .= "
                <div id=\"q\" class=\"fl\">
                     $quantita_piatto
                </div>
                
                <div id=\"piatto\" class=\"fl\">".$row_primi['descrizione']."
                	
                </div>
                
                <div id=\"prezzo\" class=\"fl\">
                € ".$prezzo_primi*$quantita_piatto." 
                </div>"
                ;
				$stampa_primi .= "</div>";
			$tot_primi = $tot_primi + ($prezzo_primi * $quantita_piatto);
		}

	}
	
			
	//$tot_primi;
	
	// fine primi	
	
	//secondi
	
	
	$stampa_secondi = "";
		
	for($j=0;$j<sizeof($a)-1;$j++){
		
		$b = explode(",",$a[$j]);
		
		$id_piatto = $b[0];
		
		$quantita_piatto = $b[1];
		
	
	
		
		$select_secondi = "SELECT * FROM piatti WHERE id_piatto=$id_piatto AND id_tipologia=3";
		
		
		$result_secondi = mysqli_query($db, $select_secondi);
		
		while($row_secondi = mysqli_fetch_array($result_secondi)){
			$stampa_secondi .= "<div id=\"riga_stampa\">";
			$prezzo_secondi = $row_secondi['prezzo'];	
			
			$stampa_secondi .= "
                <div id=\"q\" class=\"fl\">
                     $quantita_piatto
                </div>
                
                <div id=\"piatto\" class=\"fl\">".$row_secondi['descrizione']."
                	
                </div>
                
                <div id=\"prezzo\" class=\"fl\">
                € ".$prezzo_secondi*$quantita_piatto."
                </div>"
                ;
				$stampa_secondi .= "</div>";
			$tot_secondi = $tot_secondi + ($prezzo_secondi * $quantita_piatto);
		}
		
	}
	
	//$tot_secondi;
	
	
	// fine secondi
	
	//contorni
	
	$stampa_contorni = "";
	
	for($j=0;$j<sizeof($a)-1;$j++){
		
		$b = explode(",",$a[$j]);
		
		$id_piatto = $b[0];
		
		$quantita_piatto = $b[1];
		
		
		
		$select_contorni = "SELECT * FROM piatti WHERE id_piatto=$id_piatto AND id_tipologia=4";
		
		
		$result_contorni = mysqli_query($db, $select_contorni);
		
		while($row_contorni = mysqli_fetch_array($result_contorni)){
		
			$stampa_contorni .= "<div id=\"riga_stampa\">";
			$prezzo_contorni = $row_contorni['prezzo'];	
			
			$stampa_contorni .= "
                <div id=\"q\" class=\"fl\">
                     $quantita_piatto
                </div>
                
                <div id=\"piatto\" class=\"fl\">".$row_contorni['descrizione']."
                	
                </div>
                
                <div id=\"prezzo\" class=\"fl\">
                € ".$prezzo_contorni*$quantita_piatto."
                </div>"
                ;
				$stampa_contorni .= "</div>";
				
			$tot_contorni = $tot_contorni + ($row_contorni['prezzo'] * $quantita_piatto);
		}
	
	}
	
	//$tot_contorni;
	
	// fine contorni
	
	$stampa_menu = "";
	
	for($j=0;$j<sizeof($a)-1;$j++){
		
		$b = explode(",",$a[$j]);
		
		$id_piatto = $b[0];
		
		$quantita_piatto = $b[1];
		
		
		
		
		
		$select_menu = "SELECT * FROM piatti WHERE id_piatto=$id_piatto AND id_tipologia=5";
		
		
		$result_menu = mysqli_query($db, $select_menu);
		
		while($row_menu = mysqli_fetch_array($result_menu)){
		
			$stampa_menu .= "<div id=\"riga_stampa\">";
			$prezzo_menu = $row_menu['prezzo'];	
			
			$stampa_menu .= "
                <div id=\"q\" class=\"fl\">
                     $quantita_piatto
                </div>
                
                <div id=\"piatto\" class=\"fl\">".$row_menu['descrizione']."
                	
                </div>
                
                <div id=\"prezzo\" class=\"fl\">
                € ".$prezzo_menu*$quantita_piatto."
                </div>"
                ;
				$stampa_menu .= "</div>";
				
			$tot_menu = $tot_menu + ($row_menu['prezzo'] * $quantita_piatto);
		}
	
	}
	
        
        // geko
        $stampa_geko = "";
        $elenco_geko = array();
        $i = 0;
	for($j=0;$j<sizeof($a)-1;$j++){

		$b = explode(",",$a[$j]);

		$id_piatto = $b[0];

		$quantita_piatto = $b[1];
                //echo "EEEEEEEEEEEEEEEEEE ".$quantita_piatto;




		$select_geko = "SELECT * FROM piatti WHERE id_piatto=$id_piatto AND id_tipologia=6";


		$result_geko = mysqli_query($db, $select_geko);
                
		$tot_geko = 0;
		while($row_geko = mysqli_fetch_array($result_geko)){

			$stampa_geko .= "<div id=\"riga_stampa\">";
			$prezzo_geko = $row_geko['prezzo'];

			$stampa_geko .= "
                <div id=\"q\" class=\"fl\">
                     $quantita_piatto
                </div>

                <div id=\"piatto\" class=\"fl\">".$row_geko['descrizione']."

                </div>

                <div id=\"prezzo\" class=\"fl\">
                € ".$prezzo_geko*$quantita_piatto."
                </div>"
                ;
				$stampa_geko .= "</div>";

			$tot_geko += ($row_geko['prezzo'] * $quantita_piatto);

                        $elenco_geko[$i]['id_piatto'] =$id_piatto;
                        $elenco_geko[$i]['desc'] =$row_geko['descrizione'];
                        $elenco_geko[$i]['q'] =$quantita_piatto;
                        $i++;
		}

	}
	
	$t = $tot_primi+$tot_secondi+$tot_contorni+$tot_menu+$tot_geko;
	
	$numero_ordine = 'Errore: ordine non trovato';
	
	if(!$anteprima) {
    	$upd = "update ordini set tot_bevande=$tot_bevande,tot_food=$t where id_ordine=$id_ordine_inserito";
    	
    	mysqli_query($db, $upd);
    	
    	// Numero ordine
    	$select_tot = "SELECT totale AS tot FROM ordini WHERE id_ordine=$id_ordine_inserito";
    	
    	$result_tot = mysqli_query($db, $select_tot);
    	
    	while($row_tot = mysqli_fetch_array($result_tot)){
    	    
    	    $numero_ordine = $row_tot['tot'];
    	}
    	
    	// insert incasso 
    	// devo togliere lo sconto..
    	
    	if ($sconto != 0){
    		$incasso = $tot_scontato;
    		//echo $incasso;
    		
    	}else{
    	
    		$incasso = $tot_primi+$tot_secondi+$tot_contorni+$tot_bevande+$tot_menu+$tot_geko;
    		
    		//echo $incasso;
    	}
    	$select_incasso = "SELECT incasso FROM serate WHERE id_serata=$id_serata";
    	//echo $select_incasso;
    	$r = mysqli_query($db, $select_incasso);
    	
    	$inc = 0;
    	
    	while($ro=mysqli_fetch_array($r)){
    		
    		$inc = $ro['incasso'];
    		
    	}
    	
    	$incasso = $incasso + $inc;
    	
    	$insert_incasso = "UPDATE serate SET incasso=$incasso WHERE id_serata=$id_serata";
    		
    	//echo $insert_incasso;
    		
    		mysqli_query($db, $insert_incasso);
	}
	
/*************************************************************************************************
 * ############################################################################################# * TODO
 *************************************************************************************************/
		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Documento senza titolo</title>
        
        <link rel="stylesheet" href="../css/style_stampa.css?v=1.0" type="text/css" <?php //media="print"?> />
        <script>
			function stampa(){
    			window.print();
    			location.href='ordini.php';
			}
		</script>
    </head>
    
    <body <?php if (!$anteprima): ?>onload="javascript:stampa();"<?php endif; ?>>
    	<?php if ($anteprima): ?>
    	<button onclick="javascript:history.back();" class="no-stampa">Torna all'ordine</button>
    	<?php endif; ?>
     <?php if($stampa_primi != "" || $stampa_secondi != "" || $stampa_contorni != "" || $stampa_menu != "" || $stampa_geko != ""){ ?>
    <div id="principale"> 
    <div id="food">
    
  <div class="n_ordi"><?= $anteprima? 'Anterpima ordine': 'N. ordine '. $numero_ordine ?> <small><?= $asporto? 'Asporto': '' ?></small></div>
    	<div class="testa_stampa">
        	
        	<div class="relative">
	            <div class="evento">
	           		<?php echo $evento; ?>
	            </div>
            
	            <div class="data">
					dal <?php echo $inizio; ?> al <?php echo $fine; ?>
	            </div>
	        </div>
            
            <div class="relative">
	            <div class="luogo">
	           		<?php echo $luogo; ?>
	            </div>
	            
	            <div class="data">
	            
	           Serata:  <?php echo $dd[2]."-".$dd[1]."-".$dd[0]; ?>
	             </div>
	       </div>
        </div>
        
         <!-- div class="label_ordine">
        	
         </div-->	
            
          <div class="riepilogo_stampa">
        	
            
                <div class="numero">
                     
                </div>
        
        	 <div class="numero">
           		Nominativo: <?php echo $nominativo;?>
            </div>
            
             <div class="numero_r">
           		 Coperti: <?php echo $coperti;?>
            </div>
      		
        
       
        </div>
        
        
         <div class="label_food">
        	Food
         </div>
        
        <div class="contenitore_stampa">
        	
            <div class="primi_stampa">
            	<div><b>Primi</b></div>
                <?php echo $stampa_primi; ?>
            </div>
            
             <div class="primi_stampa">
             	<div><b>Secondi</b></div>
                 <?php echo $stampa_secondi; ?>
            </div>
            
             <div class="primi_stampa">
             	<div><b>Contorni</b></div>
                <?php echo $stampa_contorni; ?>
            </div>
            
            <div class="primi_stampa">
             	<div><b>Menu speciali</b></div>
                <?php echo $stampa_menu; ?>
            </div>

            <div class="primi_stampa">
             	<div><b>Menu geko</b></div>
                <?php echo $stampa_geko; ?>
            </div>
            
            <div class="tot" class="fl">
            Tot. Food € <?php echo $tot_primi+$tot_secondi+$tot_contorni+$tot_menu+$tot_geko; ?>
            </div>
            
            
        </div>
        
        <div class="contenitore_stampa_1">
       		
        	 <div class="totale_stampa">Tot. Ordine  € <?php echo $tot_bevande+$tot_primi+$tot_secondi+$tot_contorni+$tot_menu+$tot_geko; ?></div>
            <div class="totale_stampa">Sconto: <?php echo $sconto;?>%</div>
           <div class="totale_stampa">Tot. Scontato  € <?php echo $tot_scontato; ?></div>
            
    	</div>
        <br/>
        <div class="fisc">Non valida ai fini fiscali</div>
        </div>
        
        <?php } ?>
        
        <?php if (($stampa_primi != "" || $stampa_secondi != "" || $stampa_contorni != "" || $stampa_menu != "" || $stampa_geko != "") && ($stampa_bevande != "" || $stampa_menu != "" || $stampa_geko != "")) :?>
        <div class="separatore_verticale"></div>
        <?php endif; ?>
        <!-- seconda pagina-->
        
		<?php if($stampa_bevande != "" || $stampa_menu != "" || $stampa_geko != ""){ ?>
        <div id="bevande">
        
         <div class="n_ordi"><?= $anteprima? 'Anterpima ordine': 'N. ordine '. $numero_ordine ?></div>
        <div class="testa_stampa">
        	
        	<div class="relative">
	            <div class="evento">
	           		<?php echo $evento; ?>
	            </div>
            
	            <div class="data">
					dal <?php echo $inizio; ?> al <?php echo $fine; ?>
	            </div>
	        </div>
            
            <div class="relative">
	            <div class="luogo">
	           		<?php echo $luogo; ?>
	            </div>
	            
	            <div class="data">
	            
	           Serata:  <?php echo $dd[2]."-".$dd[1]."-".$dd[0]; ?>
	             </div>
	       </div>
        </div>
        
         <!-- div class="label_ordine">
        	
         </div-->	
            
          <div class="riepilogo_stampa">
        	
            
               
        
        	 <div class="numero">
           		Nominativo: <?php echo $nominativo;?>
            </div>
            
             <div class="numero_r">
           		 Coperti: <?php echo $coperti;?>
            </div>
      		
        
       
        </div>
        
        
         <div class="label_food">
        	Bevande
         </div>
        
        <div class="contenitore_stampa">
        	
            
            
             <div class="primi_stampa">
             	<div><b>Bevande</b></div>
                <?php echo $stampa_bevande; ?>
            </div>
            
            <div class="primi_stampa">
             	<div><b>Menu speciali</b></div>
                <?php echo $stampa_menu; ?>
            </div>

            <div class="primi_stampa">
             	<div><b>Menu geko</b></div>
                <?php echo $stampa_geko; ?>
            </div>
            
            <div class="tot" class="fl">
            Tot. Bevande € <?php echo $tot_bevande; ?>
            </div>
            
        </div>
        
        <div class="contenitore_stampa_1">
        	<div class="totale_stampa">Tot. Ordine  € <?php echo $tot_bevande+$tot_primi+$tot_secondi+$tot_contorni+$tot_menu+$tot_geko; ?></div>
            <div class="totale_stampa">Sconto: <?php echo $sconto;?>%</div>
            <div class="totale_stampa">Tot. Scontato  € <?php echo $tot_scontato; ?></div>
    	</div>
        
        <br/>
        <div class="fisc">Non valida ai fini fiscali</div>
        
        <div id="grazie">
            <p><b>Grazie per aver scelto la nostra festa.</b></p>
    
            <p><b>Il nostro intento è che tu possa trovare ospitalità ed amicizia.</b></p>
    
            <p><b>Scegli un tavolo ed attendi che venga chiamato il tuo numero.</b></p>
        </div>
        </div> <!-- fine div bevande (pagina dx) -->
        <?php } ?>
        <?php 
								/// fine stampa bevande ///
		?>
        <?php if($stampa_geko != ""){ ?>

        <div id="geko">

         <div class="n_ordi"><?= $anteprima? 'Anterpima ordine': 'N. ordine '. $numero_ordine ?></div>
        <div class="testa_stampa">

            <div class="evento">
           		<?php echo $evento; ?>
            </div>

            <div class="luogo">
           		<?php echo $luogo; ?>
            </div>

            <div class="data">
				dal <?php echo $inizio; ?> al <?php echo $fine; ?>
            </div>

            <div class="data">
           Serata:  <?php echo $dd[2]."-".$dd[1]."-".$dd[0]; ?>
             </div>
        </div>

         <!-- div class="label_ordine">

         </div-->

          <div class="riepilogo_stampa">




        	 <div class="numero">
           		Nominativo: <?php echo $nominativo;?>
            </div>

             <div class="numero_r">
           		 Coperti: <?php echo $coperti;?>
            </div>



        </div>


         <div class="label_food">
        	Menu geko(da compilare)
         </div>

        <div class="contenitore_stampa">



             <div class="primi_stampa">
             	
                <?php //echo $stampa_geko; ?>
                <?php
                   
                    
                    foreach($elenco_geko as $value){
                        //echo $value['desc'];
                         //echo "NNNNNNNNNNNNNN ".$value['q'];
                        $tit = explode(":",$value['desc']);
                         
                        echo "<br >";
                        echo strtoupper($tit[0]);
                        echo "<br >";
                         echo "<br >";
                        $pia = explode(",",$tit[1]);
                        foreach($pia as $value2){
                            $value2 = trim($value2);
                            
                            if($value2 == 'bigoli'){

                                echo "&nbsp;&nbsp;&nbsp;  N. ".$value['q']."  - $value2<br ><br >";
                            }else{
                                if($value2 == '1/2 acqua'){

                                    echo "&nbsp;&nbsp;&nbsp;  N. ".$value['q']." - $value2<br ><br >";
                                }else
                                if($value2 == 'cotoletta'){

                                    echo "&nbsp;&nbsp;&nbsp;  N. ".$value['q']." - $value2<br ><br >";
                                }else
                                if($value2 == 'patatine'){

                                    echo "&nbsp;&nbsp;&nbsp;  N. ".$value['q']." - $value2<br ><br >";

                                }else
                                    echo "&nbsp;&nbsp;&nbsp;  N. ____ - $value2 &nbsp;  N. ____<br ><br >";
                           }
                        }
                    }

                ?>
            </div>

            <div class="tot" class="fl">
            Tot. menu geko € <?php echo $tot_geko; ?>
            </div>

        </div>

        

        <br/>
        <div class="fisc">Non valida ai fini fiscali</div>
        
        
        </div><!--  fine div Geko -->

        <?php } ?>

        <form action="ordini.php">
        
        </form>
        </div><!--  fine div principale -->
    </body>
</html>
