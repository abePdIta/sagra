<?php

class Sagra_Mysqli extends mysqli {
	
	/**
	 * Consente di associare le variabili, in qualità di parametri, ad un'istruzione preparata.
	 * Funziona come mysqli::prepare e mysqli_stmt::bind_param messe insieme, con la differenza che
	 * preparaEInserisciParametri accetta anche parametri di tipo array.
	 * Per la stringa $tipi, si usano le stesse convenzioni di bind_param (s: stringa, i: intero, 
	 * d: precisione doppia, b: blob), facendo attenzione di utilizzare le relative maiuscole per 
	 * indicare un parametro di tipo array (S: array di stringhe, I: array di interi, ecc.).
	 * All'interno della query, si usa sempre un solo segnaposto ('?') per ciascun parametro.
	 * Es.: SELECT * FROM tabella WHERE id IN (?);
	 * @param string $query
	 * @param string $tipi
	 * @param array $parametri
	 * @return mysqli_stmt
	 */
	public function preparaEInserisciParametri($query, $tipi, array $parametri) {
		
		foreach ($parametri as $numero => $parametro){
			$parametri[$numero] = &$parametri[$numero];
		}
		/* Bind parameters. Types: s = string, i = integer, d = double,  b = blob */
		for ($i = 0; $i < strlen($tipi); $i++) {
			if ($tipi{$i} == strtoupper($tipi{$i})) {
				$parametro = [];
				foreach ($parametri[$i] as $chiave => $valore) {
					$parametro[] = &$parametri[$i][$chiave];
				}
				$lunghezza = count($parametro);
				$tipi = substr($tipi, 0, $i). str_repeat(strtolower($tipi{$i}), $lunghezza). substr($tipi, $i + 1);
				array_splice($parametri, $i, 1, $parametro);
				$posizioneSegnaposto = Sagra_Funzioni::strEnnesimaPos($query, '?', $i + 1);
				$query = substr($query, 0, $posizioneSegnaposto + 1). str_repeat(',?', $lunghezza - 1). substr($query, $posizioneSegnaposto + 1);
				
				$i += $lunghezza - 1;
			}
		}
		
		array_splice($parametri, 0, 0, $tipi);
		
		$istruzione = $this->prepare($query);
		$error = $this->error;
		if (call_user_func_array([$istruzione, 'bind_param'], $parametri)) {
			return $istruzione;
		}
		else  {
			return false;
		}
	}
	
	public function preparaInserisciEdEsegui($query, $tipi, array $parametri) {
	    ;
	}
}
/*
class Sagra_MysqliStmt {
	protected $_genitore;
	
	public function __construct(mysqli_stmt $genitore) {
		$this->_genitore = $genitore;
	}
	
	public function bind_param($tipi) {
		$argomenti = func_get_args();
		// Bind parameters. Types: s = string, i = integer, d = double,  b = blob 
		for ($i = 0; $i < strlen($tipi); $i++) {
			if ($tipi{$i} == strtoupper($tipi{i})) {
				
			}
		}
	}
	
	public function __call($nome, $argomenti) {
		switch ($nome) {
			default:
				return call_user_func_array([$this->_genitore, $nome], $argomenti);
		}
	}
	
	public function __get($nome) {
		switch ($nome) {
			default:
				return $this->_genitore->{$nome};
		}
	}
	
	public function __set($nome, $valore) {
		switch ($nome) {
			default:
				return $this->_genitore->{$nome} = $valore;
		}
	}
}

class Sagra_MysqliResult extends mysqli_result {
	 
}
*/