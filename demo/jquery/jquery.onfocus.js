(function() {
    $.fn.onfocus = function(){

        $(this).find("ul").removeClass().addClass("subMenu-off");

        $(this).find("li").focus = $(this).find("li").mouseover(function() {
            $(this).children("ul").removeClass().addClass("subMenu-on");
        });
        $(this).find("li").mouseout(function() {
            $(this).children("ul").removeClass().addClass("subMenu-off");
        });
    };
})(jQuery);

