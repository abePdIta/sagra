<?php

class Sagra_Piatto extends Sagra_Modello{
	public $id_piatto;
	public $descrizione;
	public $id_tipologia;
	public $prezzo;
	/**
	 * Vedi:
	 * @see Sagra_Piatto_Stato_Tipo
	 * @var int
	 */
	public $id_stato;
	public $contatore;
	public $giacenza;
	public $salto;
	/**
	 * Viene valorizzato nel caso di un collegamento con un Ordine
	 * @var int
	 */
	public $quantita;
}