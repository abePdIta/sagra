<?php
    // Include il nuovo sistema di classi
    require_once '../classi/Sagra_Principale.php';
	$db = null; include_once("../include/connessione.php");   /* @var $db mysqli */
	
	$id_sagra = $_GET['id_sagra'];
	
	$id_piatto = $_GET['id_piatto'];
	
	$select_s = "SELECT * FROM piatti WHERE id_piatto=$id_piatto";
	
	$result_s = mysqli_query($db, $select_s);
	
	while($row_s = mysqli_fetch_array($result_s)){
		$desc= $row_s['descrizione'];
		$prezzo = $row_s['prezzo']; 
		$id_stato = $row_s['id_stato'];
	}	
	
	

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link rel=stylesheet href="../css/style.css" type="text/css">
    </head>
    
    <body>
    	
		<?php include_once("../include/testata.php"); ?>
        
        <?php include_once("../include/menu.php"); ?>
        
        <div class="titolo_configurazione">Modifica piatto</div> 
        
        <div id="corpo_configurazione">
          
          <form action="update_piatto.php" method="post">
              
              <input type="hidden" name="id_piatto" value="<?php echo $id_piatto; ?>"  />
              
              <input type="hidden" name="id_sagra" value="<?php echo $id_sagra; ?>"  />
              	
              <div class="riga_configurazione">
                
                <div class="label_configurazione">Descrizione</div>
                
                <input type="text" name="descrizione" class="input_configurazione" value="<?php echo $desc; ?>" />
              
              </div>
              
              
               <div class="riga_configurazione">
                
                <div class="label_configurazione">Prezzo</div>
                
                <input type="text" name="prezzo" class="input_configurazione" value="<?php echo $prezzo; ?>" />
              
              </div>
              
              
               <div class="riga_configurazione">
                
                <div class="label_configurazione">Stato</div>
                
                <select name="id_stato" class="input_configurazione" >
                	<?php foreach (Sagra_Piatto_Stato_Tipo::ottScelte() as $valore => $etichetta): ?>
                	<option value="<?= $valore ?>" <?= $id_stato == $valore? 'selected="selected"': '' ?>><?= $etichetta ?></option>
                	<?php endforeach; ?>
                </select>
              
              </div>
              
              
              
              <div class="riga_configurazione">
              
                <button id="button_configurazione" type="submit">Salva</button>
              
              </div>
              
              </form>
        </div>
        
    </body>
</html>
