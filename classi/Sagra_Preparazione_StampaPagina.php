<?php

class Sagra_Preparazione_StampaPagina extends Sagra_Pagina {
	/**
	 * Ordine da stampare
	 * @var Sagra_Ordine
	 */
	public $ordine;
	
	/**
	 * Eventuale elenco filtrato dei piatti
	 * @var Sagra_Piatto[]
	 */
	public $piatti;
	
	public $azioneRiepilogo;
	
	public $tipologie;
}