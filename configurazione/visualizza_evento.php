<?php
	$db = null; include_once("../include/connessione.php");   /* @var $db mysqli */
		
	$select = "SELECT * FROM sagre WHERE id_stato = 1";
	
	$result = mysqli_query($db, $select);
	
	$descrizione = "";
	
	$luogo = "";
	
	$d_i = "";
	
	$d_f = "";
	
	$id_sagra =	"";
	
	while($row = mysqli_fetch_array($result)){
		
		$id_sagra = $row['id_sagra'];
		
		$descrizione = $row['descrizione'];
	
		$luogo = $row['luogo'];
		
		$d_i = $row['data_inizio'];
		
		$d_f = $row['data_fine'];
	
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link rel=stylesheet href="../css/style.css" type="text/css">
    </head>
    
    <body>
    	
		<?php include_once("../include/testata.php"); ?>
        
        <?php include_once("../include/menu.php"); ?>
        
        <div class="titolo_configurazione">Configurazione evento</div> 
        
        <div id="corpo_configurazione">
          
          
              
              <div class="riga_configurazione">
                
                <div class="label_configurazione">Descrizione evento</div>
                
                <div><?php echo $descrizione; ?></div>
              
              </div>
              
              
               <div class="riga_configurazione">
                
                <div class="label_configurazione">Luogo</div>
                
                <div><?php echo $luogo; ?></div>
              
              </div>
              
              
               <div class="riga_configurazione">
                
                    
                    
                        <div class="label_configurazione">Data inizio</div>
                        
                       <div><?php echo $d_i; ?></div>
                    
                    
               
               </div>
               
               <div class="riga_configurazione">  
               
              		<div class="label_configurazione">Data fine</div>
                  
                   <div><?php echo $d_f; ?></div>
              
              </div>
              
              <div class="riga_configurazione">  
               
              		<div class="label_configurazione">Azioni</div>
                  
                   <div>
                   
                       <a href="nuovo_evento.php" id="link_nuova_sagra">Nuovo</a> 
                       
                       <a href="update_evento.php" id="link_modifica_sagra">Modifica</a> 
                   
                   </div>
              
              </div>
              
              </div>
              
              
              
             
        </div>
        
    </body>
</html>
