
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link rel=stylesheet href="../css/style.css" type="text/css">
    </head>
    
    <body>
    	
		<?php include_once("../include/testata.php"); ?>
        
        <?php include_once("../include/menu.php"); ?>
        
        <div class="titolo_configurazione">Configurazione evento</div> 
        
        <div id="corpo_configurazione">
          
          <form action="insert_evento.php" method="post">
              
              <div class="riga_configurazione">
                
                <div class="label_configurazione">Descrizione evento</div>
                
                <input type="text" name="descrizione" class="input_configurazione" value="" />
              
              </div>
              
              
               <div class="riga_configurazione">
                
                <div class="label_configurazione">Luogo</div>
                
                <input type="text" name="luogo" class="input_configurazione" value="" />
              
              </div>
              
              
               <div class="riga_configurazione">
                
                <div id="contenitore_date">
                
                    <div class="label_configurazione">Data inizio</div>
                    
                    <input type="text" name="data_inizio" id="date" class="input_configurazione" value="" />
                
                </div> 
                
                <div id="contenitore_date">
                  
                    <div class="label_configurazione">Data fine</div>
                  
                    <input type="text" name="data_fine" id="date" class="input_configurazione" value="" />
              
              	</div>
                
              </div>
              
              <div class="riga_configurazione">
              
                <button id="button_configurazione" type="submit">Salva</button>
              
              </div>
              
              </form>
        </div>
        
    </body>
</html>
