<?php

/**
 * 
 * @author abe
 * @property-read Sagra_Piatto[] $piatti
 */
class Sagra_Ordine  extends Sagra_Modello{
	public $id_ordine;
	public $nominativo;
	public $coperti;
	/**
	 * Numero ordine
	 * @var int
	 */
	public $totale;
	public $tot_bevande;
	public $tot_food;
	public $id_serata;
	public $evaso;
	public $asporto = false;
	
	protected $_piatti;
	
	const OTT_PIATTI_SQL = 'SELECT piatti.*, piatti_ordini.quantita FROM piatti LEFT JOIN piatti_ordini USING (id_piatto) LEFT JOIN ordini USING (id_ordine) WHERE id_ordine = ? ORDER BY id_tipologia, descrizione';
	
	public function __get($nome) {
		switch ($nome) {
			case 'piatti':
				if (!isset($this->_piatti)) {
					$this->_piatti = $this->_ottModelliCorrelati(self::OTT_PIATTI_SQL, 'i', [$this->id_ordine], Sagra_Piatto::class);
				}
				return $this->_piatti;
				
			default:
				throw new Exception(sprintf('Si sta cercando di accedere ad una proprietà (%s) che non esiste per la classe %s.', $nome, __CLASS__));
		}
	}
}