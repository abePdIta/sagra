-- Aggiungi tabella log
CREATE TABLE `messaggi_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `livello` enum('EMERG','ALERT','CRIT','ERR','WARN','NOTICE','INFO','DEBUG') NOT NULL DEFAULT 'DEBUG',
  `messaggio` varchar(255) NOT NULL,
  `extra` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `messaggi_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `livello` (`livello`);

ALTER TABLE `messaggi_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;


-- Tabella serate, colonne fondo_cassa e incasso hanno finalmente un valore di default
ALTER TABLE `serate` CHANGE `fondo_cassa` `fondo_cassa` DOUBLE UNSIGNED NOT NULL DEFAULT '0', CHANGE `incasso` `incasso` DOUBLE NOT NULL DEFAULT '0';

  
  


-- aggiornamento numero versione
UPDATE `variabili` SET `valore` = '003' WHERE `variabili`.`nome` = 'versione-database';
