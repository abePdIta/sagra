<?php
	
	$id_sagra = 0;
	
	$db = null; include_once("../include/connessione.php");   /* @var $db mysqli */
	
	include_once("../classi/sagra.php");
	
	include_once("../include/sagra_corrente.php");
	
	
	
	$select_primi = "SELECT * FROM piatti WHERE id_tipologia = 2";
	
	$result_primi = mysqli_query($db, $select_primi, $db);
			
	$i = 0;
	
	$primi = "<div id=\"primi\">
	
				<div id=\"titolo_riga_menu\">Primi</div>
	";
                        
                    
	while($row_primi = mysqli_fetch_array($result_primi)){
		
		$p1 = $row_primi['id_piatto'];
		
		$primi .= "
		
		<div id=\"riga_menu\">
					
				<div class=\"piu_c\" id=\"piu_$p1\">
					<img src=\"../img/piu.png\" class=\"piu\" id=\"piu_".$p1."\" />
				</div>
			
				<div class=\"meno_c\" id=\"meno_$p1\">
					<img src=\"../img/meno.png\" class=\"meno\" id=\"meno_".$p1."\" />
				</div>
				
				<div id=\"descrizione\">
				
				".$row_primi['descrizione']."
				
				</div>
				
				
				
				<div id=\"quantita\">
					<input type=\"text\" name=\"quantita_c_".$p1."\" id=\"quantita_".$p1."\" class=\"input_quantita\"  value=\"0\"/>
					
				</div>
				
				<div class=\"prezzo\" id=\"prezzo_".$p1."\">
					".$row_primi['prezzo']." €
				</div>
				
				
		</div>";
		
		
	}
	
	$primi .="</div>";


	$select_secondi = "SELECT * FROM piatti WHERE id_tipologia = 3";
	
	$result_secondi = mysqli_query($db, $select_secondi, $db);
			
	$i = 0;
	
	$secondi = "<div id=\"secondi\"><div id=\"titolo_riga_menu\">Secondi</div>";
                        
	                
	while($row_secondi = mysqli_fetch_array($result_secondi)){
			
			$p2 = $row_secondi['id_piatto'];
	
			$secondi .= "
			
			<div id=\"riga_menu\">
				
				<div class=\"piu_c\" id=\"piu_$p2\">
					<img src=\"../img/piu.png\" class=\"piu\" id=\"piu_".$p2."\" />
				</div>
			
				<div class=\"meno_c\" id=\"meno_$p2\">
					<img src=\"../img/meno.png\" class=\"meno\" id=\"meno_".$p2."\" />
				</div>
				
				<div id=\"descrizione\">
				
					".$row_secondi['descrizione']."
				</div>
				
				
				
				<div id=\"quantita\">
					<input type=\"text\" name=\"quantita_c_".$p2."\" id=\"quantita_".$p2."\" class=\"input_quantita\"  value=\"0\"/>
					
				</div>
				
				<div class=\"prezzo\" id=\"prezzo_".$p2."\">
					".$row_secondi['prezzo']." €
				</div>
			</div>";
		
		$i++;
		
	}
	
	
	$secondi .="</div>";
	
	$select_contorni = "SELECT * FROM piatti WHERE id_tipologia = 4";
	
	$result_contorni = mysqli_query($db, $select_contorni, $db);
			
	$i = 0;
	
	$contorni = "<div id=\"contorni\"><div id=\"titolo_riga_menu\">Contorni</div>";
                        
                    
	while($row_contorni = mysqli_fetch_array($result_contorni)){
		
		$p3 = $row_contorni['id_piatto'];
		
		$contorni .= "
		
		<div id=\"riga_menu\">
		
				<div class=\"piu_c\" id=\"piu_$p3\">
					<img src=\"../img/piu.png\" class=\"piu\" id=\"piu_".$p3."\" />
				</div>
			
				<div class=\"meno_c\" id=\"meno_$p3\">
					<img src=\"../img/meno.png\" class=\"meno\" id=\"meno_".$p3."\" />
				</div>
				
				<div id=\"descrizione\">
		
					".$row_contorni['descrizione']."
					
					</div>
				
				
				
				<div id=\"quantita\">
					<input type=\"text\" name=\"quantita_c_".$p3."\" id=\"quantita_".$p3."\" class=\"input_quantita\"  value=\"0\"/>
					
				</div>
				
				<div class=\"prezzo\" id=\"prezzo_".$p3."\">
					".$row_contorni['prezzo']." €
				</div>
				
		</div>";
		
		
	}
	
	$contorni .="</div>";
	
	
	
	$select_bevande = "SELECT * FROM piatti WHERE id_tipologia = 1";
	
	$result_bevande = mysqli_query($db, $select_bevande, $db);
			
	$i = 0;
	
	$bevande = "<div id=\"bevande\"><div id=\"titolo_riga_menu\">bevande</div>";
                        
                    
	while($row_bevande = mysqli_fetch_array($result_bevande)){
		
		$p3 = $row_bevande['id_piatto'];
		
		$bevande .= "
		
		<div id=\"riga_menu\">
		
				<div class=\"piu_c\" id=\"piu_$p3\">
					<img src=\"../img/piu.png\" class=\"piu\" id=\"piu_".$p3."\" />
				</div>
			
				<div class=\"meno_c\" id=\"meno_$p3\">
					<img src=\"../img/meno.png\" class=\"meno\" id=\"meno_".$p3."\" />
				</div>
				
				<div id=\"descrizione\">
		
					".$row_bevande['descrizione']."
					
					</div>
				
				
				
				<div id=\"quantita\">
					<input type=\"text\" name=\"quantita_c_".$p3."\" id=\"quantita_".$p3."\" class=\"input_quantita\"  value=\"0\"/>
					
				</div>
				
				<div class=\"prezzo\" id=\"prezzo_".$p3."\">
					".$row_bevande['prezzo']." €
				</div>
				
		</div>";
		
		
	}
	
	$bevande .="</div>";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link rel=stylesheet href="../css/style.css" type="text/css">
        
        <script src="../jquery/jquery.js" type="text/javascript"></script>
        
        <script type="text/javascript">
			
			$(document).ready(function(){
				
				$("#input_nominativo").focus();
			
				$("#bevande").css({display:"none"});
				
				$(".piu").mouseover(function(){
				
					$(this).css({cursor:"pointer"});
					
				});
				
				$(".meno").mouseover(function(){
				
					$(this).css({cursor:"pointer"});
					
				});
				
				$("#bottone_totale").mouseover(function(){
				
					$(this).css({cursor:"pointer"});
					
				});
				
				
				$("#bottone_azzera").mouseover(function(){
				
					$(this).css({cursor:"pointer"});
					
				});
				
				
				$("#bottone_stampa").mouseover(function(){
				
					$(this).css({cursor:"pointer"});
					
				});
				
				$("#bottone_azzera").click(function(){
				
					$("input").val(0);
					
				});
				
				
				$("#bottone_totale").click(function(){
					
					var id_piatto = new Array();
					
					var q = new Array();
					
					var i = 0;
					
					var stringa ="";
					
					$(".input_quantita").each(function(){
						
						var id_q = $(this).attr("id"); 
						
						
						
						id_q = id_q.substr(9);
						
						
						var v = $(this).val();
						
						if(v > 0){
					
							stringa = stringa +id_q+","+v+";";
							
						}
						
						i++;
					
					});
					
					
					
						$("#totale").load("calcola.php", {st:stringa}, function(data){
  							$("#totale_span").html(data);
							$("#totale_hidden").val(data);
 						});
					
				});
				
				
				$("#bottone_sconto").click(function(){
					
					var percentuale =prompt("Inserire la percentuale di sconto(senza %)");
					
					var totale = $("#totale_hidden").val();
					//alert(totale);
					
					$.post("calcola_sconto.php",{percentuale:percentuale,totale:totale},function(data){
							
							$("#totale_span").html(data);
							$("#totale_hidden").val(data);
						});
						
				});
				
				$("#bottone_stampa").click(function(){
					
					var id_piatto = new Array();
					
					var q = new Array();
					
					var i = 0;
					
					var stringa ="";
					
					$(".input_quantita").each(function(){
						
						var id_q = $(this).attr("id"); 
						
						
						
						id_q = id_q.substr(9);
						
						
						var v = $(this).val();
						
						if(v > 0){
					
							stringa = stringa +id_q+","+v+";";
							
						}
						
						i++;
					
					});
					
					/*stringa = stringa + "id:"+i;*/
					
					$("#st").val(stringa);
					
					var n = $("#input_nominativo").val();
					alert(n);
					$("#nomintativo_hidden").val(n);
					alert($("#nomintativo_hidden").val());
					$("#form_stampa").submit();
				
				});
				
				
				$(".piu").click(function(){
						
						var id = $(this).attr("id");
						
						id = id.substring(4)
						
						var id_q = "quantita_"+ id;
						
						var t = $("#"+id_q).val();
						
						t++;
						
						$("#"+id_q).val(t);
						
					});
					
					$(".meno").click(function(){
						
						var id = $(this).attr("id");
						
						id = id.substring(5)
						
						var id_q = "quantita_"+ id;
						
						var t = $("#"+id_q).val();
						
						t--;
						
						$("#"+id_q).val(t);
						
					});
					
					
					$("#link_bevande").mouseover(function(){
				
					$(this).css({cursor:"pointer"});
					
					});
					
					$("#link_food").mouseover(function(){
					
						$(this).css({cursor:"pointer"});
						
					});
					
					$("#link_bevande").click(function (){
						
						$("#food").css({display:"none"});
						$("#bevande").css({display:"block"});
						$("#link_bevande").css({background:"#A6B6CE"});
						$("#link_food").css({background:"#E1E8EC"});
						
						
					});
					
					$("#link_food").click(function (){
						
						
						$("#bevande").css({display:"none"});
						$("#food").css({display:"block"});
						$("#link_food").css({background:"#A6B6CE"});
						$("#link_bevande").css({background:"#E1E8EC"});
						
					});
			
			});
		
		</script>
    </head>
    
    <body>
    	
		<?php include_once("../include/testata.php"); ?>
        
        <?php include_once("../include/menu.php"); ?>
        
        <!-- id ordine, nominativo-->
        
        <div id="testa">
        
        	<div id="id_ordine"></div>
            
            <div id="nominativo">Nominativo:<input type="text" name="input_nominativo" id="input_nominativo"  /></div>
            
            <div id="totale">
            	€ <span id="totale_span"></span>
            </div>
            
        </div>
        
                        
                    
        
        <div id="contenitore_ordini">
        
            <div id="contatori">
              ss
           
            </div>
            
            <!-- Container tabs-->
            <div id="corpo_ordini">
             	
                <div id="prima_riga">
                
                	<div id="contenitore_tab">
                    	<ul id="tab">

                            <li class="li_tab" ><div class="border_li" id="link_food"><b>Food</b></div></li>
                            
                            <li class="li_tab" ><div class="border_li" id="link_bevande"><b>Bevande</b></div></li>
                           
                        </ul>
                    </div>
                </div>
                
                <div id="food">
                
                    <div id="prima_riga">
                    
                        <?php echo $primi; ?>
                        
                        <?php echo $contorni; ?>
                        
                    </div>
                    
                    <div id="prima_riga">
                    
                        <?php echo $secondi; ?>
                 
                    </div>
               </div>
               
               <div id="bevande">
               
               	<div id="prima_riga">
                    
                        <?php echo $bevande; ?>
                 
                    </div>
               </div>
               
            </div>
            <!-- fine container -->
            <div id="pulsanti">
              
              <div id="bottone_totale">
              		Totale
              </div>
           	
            	<div id="bottone_sconto">
              		Sconto
              </div>	
              <div id="bottone_stampa">
              		
                    <form name="form_stampa" id="form_stampa" action="stampa.php" method="post">
                    
                    	<input type="hidden" id="st" name="st" />
                        
                       <input type="hidden" id="nomintativo_hidden" name="nominativo_hidden" value="" />
                        
                        <input type="hidden" id="totale_hidden" name="totale_hidden" value="" />
                        
                        
                        
                    	Stampa
                    
                    </form>
              </div>  
              
              <div id="bottone_azzera">
              		Azzera
              </div>  
            </div>
        
        </div>
        
    </body>
</html>
