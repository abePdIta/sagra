<?php

class Sagra_Preparazione_IndexPagina extends Sagra_Pagina {
	/**
	 * Numero ordini ancora da preparare per questo reparto
	 * @var integer
	 */
	public $totaleOrdiniRimanenti;
	
	/**
	 * Dettaglio degli ordini ancora da preparare
	 * @var Sagra_Preparazione_OrdineInCoda[]
	 */
	public $arrayOrdiniInCoda;
	
	/**
	 * Dettaglio degli ordini per asporto ancora da preparare
	 * @var Sagra_Preparazione_OrdineInCoda[]
	 */
	public $arrayOrdiniAsportoInCoda;
	
	/**
	 * Porzioni in coda e preparate
	 * @var Sagra_Preparazione_Piatti[]
	 */
	public $arrayPiatti;
	
	public $azioneStampa;
	
	public $tipologie;
}