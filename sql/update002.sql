-- Eliminato evaso da ordini per metterlo in piatti_ordini
ALTER TABLE `ordini` DROP `evaso`;
ALTER TABLE `piatti_ordini`  ADD `evaso` BOOLEAN NOT NULL DEFAULT FALSE  AFTER `quantita`,
  ADD   INDEX  (`evaso`);

UPDATE `piatti_ordini` SET `evaso` = TRUE;





-- aggiornamento numero versione
UPDATE `variabili` SET `valore` = '002' WHERE `variabili`.`nome` = 'versione-database';
