<?php
class Sagra_Pagina {
	protected $_layout = 'default';
	
	protected $_percorsoLayout = 'include/layout';
	
	protected $_titolo;
	
	protected $_scriptVista;
	
	protected $_javascript = [];
	
	
	
    public function rendi() {
    	$fileLayout = Sagra_Principale::cercaFileRisalendoStruttura($this->ottLayout(). '.phtml', [$this->_percorsoLayout]);
    	
    	if (false === $fileLayout) {
    		throw new Exception('Impossibile trovare il layout «'. $this->ottLayout(). '». La ricerca è stata effettuata in: "'. $this->_percorsoLayout. '".');
    	}
    	
    	include $fileLayout;
    }
    
    public function impLayout($nomeLayout) {
    	$this->_layout = (string) $nomeLayout;
    	return $this;
    }
    
    public function ottLayout() {
    	return $this->_layout;
    }
    
    public function impTitolo($titolo) {
    	$this->_titolo = (string) $titolo;
    	return $this;
    }
    
    public function ottTitolo() {
    	return $this->_titolo;
    }
    
    public function inserisciContenutoPrincipale() {
        if (isset($this->_scriptVista)) {
            $nomeScriptPhtml = $this->_scriptVista;
        }
        else {
        	$backtrace = debug_backtrace(false);
        	$primoLivello = array_pop($backtrace);
        	$infoPercorso = pathinfo($primoLivello['file']);
        	$nomeScriptPhtml = $infoPercorso['dirname']. '/'. $infoPercorso['filename']. '.phtml';
        }
    	
    	if (!file_exists($nomeScriptPhtml)) {
    		throw new Exception('Impossibile trovare lo script di pagina al percorso: '. $nomeScriptPhtml);
    	}
    	
    	include $nomeScriptPhtml;
    }
    
    public function inserisciJavascript() {
        foreach ($this->_javascript as $script) {
            echo '<script type="text/javascript">'. $script. '</script>'. PHP_EOL;
        }
    }
    
    public function aggiungiJavascript($script) {
        $this->_javascript[] = (string)$script;
        return $this;
    }
    
    public function impScriptVista($nomeFile) {
        $this->_scriptVista = (string) $nomeFile;
        return $this;
    }
    
    public static function ottParametro($nome, $default = NULL) {
    	if (isset($_GET[$nome])) {
    		return $_GET[$nome];
    	}
    	elseif (isset($_POST[$nome])) {
    		return $_POST[$nome];
    	}
    	else {
    		return $default;
    	}
    }
}