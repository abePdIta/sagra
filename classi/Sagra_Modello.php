<?php

class Sagra_Modello {
	protected function _ottModelliCorrelati ($querySql, $tipi, array $parametri, $nomeClasseModello) {
		$istruzione = Sagra_Principale::ottCollegamentoBasedati()->preparaEInserisciParametri($querySql, $tipi, $parametri);
		$istruzione->execute();
		
		$risultato = $istruzione->get_result();	/* @var $risultato mysqli_result */
		
		$daRestituire = [];
		while ($modello = $risultato->fetch_object($nomeClasseModello)) {
			$daRestituire[] = $modello;
		}
		return $daRestituire;
	}
}