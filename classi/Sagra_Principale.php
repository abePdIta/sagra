<?php

/**
 * Classe principale (chi l'avrebbe detto) per gestire l'ambaradan.
 * Il file di questa classe è l'unico che deve essere incluso manualmente (usando "require_once", ad esempio);
 * tutte le altre classi verranno caricate dinamicamente, perché Sagra_Principale gestisce anche l'autoload.
 * @author abe
 */
class Sagra_Principale {
	
	const POSIZIONE_FILE_CONFIGURAZIONE = '../include/config.inc.php';
	
	const DOVE_CERCARE_CLASSI = ['.', 'classi', 'classi/tipi']; 
	
	
	protected static $_configurazione;
	
	protected static $_collegamentoBasedati;
	
	protected static $_idSagra;
	
	protected static $_idSerataCorrente;
	
	public static function cercaFileRisalendoStruttura ($nomeFile, $directoriesInCuiCercare = ['.']) {
		// Quanto possiamo risalire la struttura delle directori?
		$backtrace = debug_backtrace(false);
		$fileDiPartenza = array_pop($backtrace)['file'];
		$profonditaStruttura = count(explode('/', $fileDiPartenza));
		
		foreach ($directoriesInCuiCercare as $dir) {
			// Non sappiamo dove ci troviamo: risaliamo la struttura delle directory, finché non troviamo una delle directory in cui cercare.
			while (!(file_exists($dir) && is_dir($dir))) {
				$dir = '../'. $dir;
				$profonditaStruttura--;
				
				if ($profonditaStruttura < 2) {
					break;
				}
			}
			
			$ipotesiNomeFile = $dir. '/'. $nomeFile;
			
			if (file_exists($ipotesiNomeFile)) {
				return ($ipotesiNomeFile);
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @return string[]
	 */
	public static function ottConfigurazione() {
		if (!isset(self::$_configurazione)) {	
			require_once self::POSIZIONE_FILE_CONFIGURAZIONE;
			
			self::$_configurazione = [
					'nomeBasedati' => $db_name,
					'utenteBasedati' => $db_user,
					'passwordBasedati' => $db_password,
					'hostBasedati' => $db_host,
			];
		}
		return self::$_configurazione;
	}
	
	/**
	 * 
	 * @throws mysqli_sql_exception
	 * @return Sagra_Mysqli
	 */
	public static function ottCollegamentoBasedati() {
		if (!isset(self::$_collegamentoBasedati)) {
			$config = self::ottConfigurazione();
	
			
			$db = new Sagra_Mysqli($config['hostBasedati'], $config['utenteBasedati'], $config['passwordBasedati'], $config['nomeBasedati']);
			
			if ($db === FALSE) throw new mysqli_sql_exception('Impossibile connettersi alla base dati');
			
			$db->query("SET NAMES 'utf8'");
			
			self::$_collegamentoBasedati = $db;
		}
		return self::$_collegamentoBasedati;
	}
	
	public static function ottIdSagra() {
		if (!isset(self::$_idSagra)) {
			$id_sagra = null;
			
			$query = "SELECT id_sagra FROM sagre WHERE id_stato = 1";
			$result = self::ottCollegamentoBasedati()->query($query);
			
			while($row = $result->fetch_array()){
				$id_sagra = $row['id_sagra'];
			}
			
			self::$_idSagra = $id_sagra;
		}
		return self::$_idSagra;
	}
	
	public static function ottIdSerataCorrente() {
		if (!isset(self::$_idSerataCorrente)) {
			$id_serata = null;
			
			$data_corrente = date("Y-m-d");
			
			$select_serata = "SELECT id_serata FROM serate WHERE data = '$data_corrente' AND id_sagra = '". self::ottIdSagra(). "'";
			
			$result_serata = self::ottCollegamentoBasedati()->query($select_serata);
			
			
			while($row_serata = $result_serata->fetch_array()){
				$id_serata = $row_serata['id_serata'];
			}
			
			self::$_idSerataCorrente = $id_serata;
		}
		return self::$_idSerataCorrente;
	}
}

// Configuriamo il caricamento automatico delle classi
spl_autoload_register(function ($nomeClasse) {
	// Prima vediamo se la classe esiste nella directory corrente, poi cerchiamo in "classi"
	$directoriesInCuiCercare = Sagra_Principale::DOVE_CERCARE_CLASSI;
	
	$trovato = Sagra_Principale::cercaFileRisalendoStruttura($nomeClasse. '.php', $directoriesInCuiCercare);
	
	if (false === $trovato) {
		throw new Exception('Impossibile trovare la classe «'. $nomeClasse. '». La ricerca è stata effettuata in: "'. implode('", "', $directoriesInCuiCercare). '".');
	}
	
	require_once $trovato;
});