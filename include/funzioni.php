<?php
require_once '../classi/Sagra_Pagina.php';

class Funzioni {
	public static function getParameter($name, $default = null) {
		return Sagra_Pagina::ottParametro($name, $default);
	}
}