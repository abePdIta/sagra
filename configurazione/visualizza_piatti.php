<?php

    // Include il nuovo sistema di classi
    require_once '../classi/Sagra_Principale.php';
	$db = null; include_once("../include/connessione.php");   /* @var $db mysqli */
	
	$id_sagra = $_GET['id'];
	
	$select = "SELECT * FROM piatti 
				INNER JOIN tipologie ON tipologie.id_tipologia=piatti.id_tipologia
			   ORDER BY piatti.id_tipologia";
	
	//echo $select;
	
	$result = mysqli_query($db, $select);
	
	$piatti_html = "";
	
	$i=1;
	
	while($row = mysqli_fetch_array($result)){
		
	    $disattivato = $row['id_stato'] == Sagra_Piatto_Stato_Tipo::DISATTIVATO? 'disattivata': '';
		$piatti_html .= "
						<div class=\"riga_configurazione_serata $disattivato\">
							<div id=\"n_serata\" class=\"in_linea\">".$i."</div>
       			
							
						
							 <div id=\"desc_piatti\" class=\"in_linea\">".$row['descrizione']."</div>
							
							<div id=\"prezzo\" class=\"in_linea\">".$row['prezzo']."</div>
							
							<div id=\"tipo_piatto\" class=\"in_linea\">".$row['tipologia']."</div>
						
							<div id=\"azioni_piatti\" class=\"in_linea\">
								
								<a href=\"modifica_piatto.php?id_sagra=".$id_sagra."&id_piatto=".$row['id_piatto']."\">Modifica</a>
								
								<a href=\"elimina_piatto.php?id_sagra=".$id_sagra."&id_piatto=".$row['id_piatto']."\">Elimina</a>
							</div>
						
						</div>
		
						";
						
			$i++;
		
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link rel=stylesheet href="../css/style.css?v=1.0" type="text/css">
        
       
    </head>
    
    <body>
    	
		<?php include_once("../include/testata.php"); ?>
        
        <?php include_once("../include/menu.php"); ?>
        
        <!-- id ordine, nominativo-->
        
       <div class="titolo_configurazione_piatti">Elenco piatti</div> 
        
       <div id="corpo_configurazione_piatti">
        
        	<div class="riga_configurazione_serata">
            	
                <div id="titolo_n_serata" class="in_linea_titolo">N.</div>
                <div id="titolo_desc_piatti" class="in_linea_titolo">Descrizione</div>
                <div id="titolo_prezzo" class="in_linea_titolo">€</div>
                <div id="titolo_tipo_piatto" class="in_linea_titolo">Tipologia</div>
                
                <div id="titolo_azioni_piatti" class="in_linea_titolo">Azioni</div> 
        	
            </div>
            
			<?php echo $piatti_html; ?> 
        	
            <br /><br /><br />
            
            <div>
            <a href="nuovo_piatto.php?id=<?php echo $id_sagra;?>">Nuovo piatto</a>
            </div>
        </div>
        
    </body>
</html>
