<?php

/**
 * 
 * @author abe
 *
 * @method static void debug($messaggio, array $extra) Aggiunge un messaggio di log.
 * @method static void info($messaggio, array $extra) Aggiunge un messaggio di log.
 * @method static void notice($messaggio, array $extra) Aggiunge un messaggio di log.
 * @method static void warn($messaggio, array $extra) Aggiunge un messaggio di log.
 * @method static void err($messaggio, array $extra) Aggiunge un messaggio di log.
 * @method static void crit($messaggio, array $extra) Aggiunge un messaggio di log.
 * @method static void alert($messaggio, array $extra) Aggiunge un messaggio di log.
 * @method static void emerg($messaggio, array $extra) Aggiunge un messaggio di log.
 */
class Sagra_Log
{
    const LIVELLO_DEBUG = 'debug';
    const LIVELLO_INFO = 'info';
    const LIVELLO_NOTICE = 'notice';
    const LIVELLO_WARN = 'warn';
    const LIVELLO_ERR = 'err';
    const LIVELLO_CRIT = 'crit';
    const LIVELLO_ALERT = 'alert';
    const LIVELLO_EMERG = 'emerg';
        
    public function __callstatic($nome, $argomenti)
    {
        switch ($nome) {
            case 'debug':
            case 'info':
            case 'notice':
            case 'warn':
            case 'err':
            case 'crit':
            case 'alert':
            case 'emerg':
                if (count($argomenti) < 2) throw new RuntimeException(__CLASS__. '::'. $nome. ': invocato senza abbastanza argomenti.');
                
                self::messaggioLog($argomenti[0], $argomenti[1], strtoupper($nome));
        }
    }
    
    public static function messaggioLog($messaggio, array $extra, $livello = self::LIVELLO_DEBUG)
    {
        $extra = json_encode($extra);
        $bd = Sagra_Principale::ottCollegamentoBasedati();
        $query = 'INSERT INTO messaggi_log (livello, messaggio, extra) VALUES (?, ?, ?)';
        $istruzione = $bd->preparaEInserisciParametri($query, 'sss', [$livello, $messaggio, $extra]);
        
        $istruzione->execute();
        $istruzione->close();
    }
}

