<?php
	
	$db = null; include_once("../include/connessione.php");   /* @var $db mysqli */
	
	$id_sagra = $_GET['id_sagra'];
	
	$id_serata = $_GET['id_serata'];
	
	$select = "SELECT * FROM contatori_serate 
	INNER JOIN piatti ON piatti.id_piatto=contatori_serate.id_piatto
				WHERE id_serata = $id_serata and contatori_serate.id_stato=1";
	
	//echo $select;
	
	$result = mysqli_query($db, $select);
	
	$piatti_html = "";
	
	$i=1;
	
	while($row = mysqli_fetch_array($result)){
		
		$piatti_html .= "
						<div class=\"riga_configurazione_serata\">
							<div id=\"n_serata\" class=\"in_linea\">".$i."</div>
       			
							
						
							 <div id=\"desc_piatti\" class=\"in_linea\">".$row['descrizione']."</div>
							
							<div id=\"prezzo\" class=\"in_linea\">".$row['magazzino']."</div>
							
							
						
							<div id=\"azioni_piatti\" class=\"in_linea\">
								
								
								
								<a href=\"elimina_contatore.php?id_sagra=".$id_sagra."&id_piatto=".$row['id_piatto']."&id_serata=".$row['id_serata']."\">Elimina</a>
							</div>
						
						</div>
		
						";
						
			$i++;
		
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link rel=stylesheet href="../css/style.css" type="text/css">
        
       
    </head>
    
    <body>
    	
		<?php include_once("../include/testata.php"); ?>
        
        <?php include_once("../include/menu.php"); ?>
        
        <!-- id ordine, nominativo-->
        
       <div class="titolo_configurazione_piatti">Elenco contatori</div> 
        
       <div id="corpo_configurazione_piatti">
        
        	<div class="riga_configurazione_serata">
            	
                <div id="titolo_n_serata" class="in_linea_titolo">N.</div>
                <div id="titolo_desc_piatti" class="in_linea_titolo">Descrizione</div>
                <div id="titolo_prezzo" class="in_linea_titolo">Magazzino</div>
                
                
                <div id="titolo_azioni_piatti" class="in_linea_titolo">Azioni</div> 
        	
            </div>
            
			<?php echo $piatti_html; ?> 
        	
            <br /><br /><br />
            
            <div>
            <a href="nuovo_contatore.php?id=<?php echo $id_sagra;?>&id_serata=<?php echo $id_serata;?>">Nuovo contatore</a>
            </div>
        </div>
        
    </body>
</html>
